# Kolay Git

This app is my first tkinter app. I use this to pull and push my repos in an easier way. 

### Special thanks to

* [Hara Gopal for his tkinter tutorial](https://www.udemy.com/building-cross-platform-python-gui-applications-with-tkinter/)
* [buildwithpython for his tutorials on youtube](https://www.youtube.com/channel/UCirPbvoHzD78Lnyll6YYUpg)