import os
import time
import subprocess
from tkinter import Tk, ttk, Label, IntVar, StringVar, filedialog, Toplevel
from threading import Thread

import sys
sys.path.insert(0, "pygit_package")

try:
    from pygit_package import pygit
except ImportError:
    # print('no import')
    pass

"""
App starts here
"""

class AutoGit:

    def __init__(self,master):
        self.master = master
        # Title frame and label
        self.title_frame = ttk.Frame(self.master, width=600, relief=None)
        self.title_frame.pack(side='top', fill='x', anchor='nw', ipadx=10)
        # self.title_frame.pack_propagate(False)

        self.title_lable = ttk.Label(self.title_frame, text='Kolay Git', font=('helvetica', 14))
        self.title_lable.pack(anchor='w', padx=10, pady=(10, 5))

        # Display Folders frame and labels
        self.folders_frame = ttk.LabelFrame(self.master, text='Seçilen klasör içeriği', width=600, height=355, relief='solid')
        self.folders_frame.pack(side='top', fill='both', expand=True, anchor='sw', padx=10, pady=(10,0), ipadx=10, ipady=10)
        self.folders_frame.pack_propagate(False)



        # Display Folders frame and labels
        self.buttons_frame = ttk.Frame(self.master, width=600, height=30)
        self.buttons_frame.pack(side='top', fill='x', ipadx=10, ipady=5, padx=10, anchor='nw')
        # self.buttons_frame.pack_propagate(False)

        self.open_folder = ttk.Button(self.buttons_frame, text='Klasör Seçin', command=self.openFolder)
        self.open_folder.grid(row=0, column=0, padx=(0, 10), pady=(10, 5))

        self.btn_pull = ttk.Button(self.buttons_frame, text='Git Pull', command=self.gitPull)
        self.btn_pull.grid(row=0, column=1, padx=(0, 10), pady=(10, 5))

        self.btn_pull_push = ttk.Button(self.buttons_frame, text='Git Pull % Push', command=self.gitPullPush)
        self.btn_pull_push.grid(row=0, column=2, padx=(0, 10), pady=(10, 5))

        self.buttons_label = ttk.Label(self.buttons_frame, font=('helvetica', 11))
        self.buttons_label.grid(row=0, column=3, padx=(0, 10), pady=(10, 5), ipadx=5, ipady=2)

    def openFolder(self):
        try:
            self.folder = filedialog.askdirectory(title='Git Repo Klasörünü Açın', initialdir='D:\\Python')
            self.repo_list = os.listdir(self.folder)

            self.loadRepos()
        except FileNotFoundError:
            self.pop_up = Toplevel(self.master)
            self.pop_up.title('Hata oluştu')
            self.pop_up.geometry('300x125+320+280')
            self.pop_up.resizable(False, False)
            self.pop_up.lift(master)
            ttk.Label(self.pop_up, text='Belirtilen yolda herhangi bir dosya bulunamadı.',
                      wraplength=290,
                      background='light blue').pack(padx=10, pady=25)
            ttk.Button(self.pop_up, text='Tamam', command=self.pop_up.destroy).pack(padx=10, pady=5)

    def loadRepos(self):
        # print(self.repo_list)
        self.int_var = dict()
        self.str_var = dict()
        row_num = 0
        self.new_repo_list = []
        for f in self.repo_list:
            try:
                git = pygit.PyGit(f)
                if git.checkRepo():
                    self.int_var[f] = IntVar()
                    cb = ttk.Checkbutton(self.folders_frame, text=f, variable=self.int_var[f], width=20)
                    cb.grid(row=row_num, column=0, padx=10, pady=5, ipadx=2, ipady=2)
                    self.new_repo_list.append(f)
                    row_num += 1
            except Exception:
                # print(Exception, 'bir hata oluştu')
                pass

        row_num = 0
        for f in self.new_repo_list:
            try:
                git = pygit.PyGit(f)
                if git.checkRepo():
                    self.str_var[f] = StringVar()
                    ent = ttk.Entry(self.folders_frame, textvariable=self.str_var[f], width=70)
                    ent.grid(row=row_num, column=1, padx=10, pady=5, ipadx=2, ipady=2)
                    row_num += 1

            except Exception:
                # print(Exception, 'bir hata oluştu')
                pass

    def gitPull(self):
        self.buttons_label.config(text="Lütfen bekleyin...", background='yellow')
        self.btn_pull.state(['disabled'])
        t1 = Thread(target=self.pullHelper, args=(self.new_repo_list,))
        t1.start()

    def pullHelper(self, repo):

        for f in repo:
            try:
                git = pygit.PyGit(f)
                m = git.pull().strip()
                if m == 'Already up to date.':
                    message = f + ' zaten güncellenmiş'
                    bg_color = '#ffcc00'
                    fg_color = 'black'
                else:
                    message = f + ' güncellendi.'
                    bg_color = '#b6ff00'
                    fg_color = 'black'
                self.buttons_label['text']= message
                self.buttons_label['background'] = bg_color,
                self.buttons_label['foreground']=fg_color
            except:
                message = f"{f} bir git klasörü değil."
                self.buttons_label.config(text=message)

        self.buttons_label.config(text="İşlem tamamlandı!", background='#069622', foreground='#FFFFFF')
        self.btn_pull.state(['!disabled'])

    def gitPullPush(self):
        self.buttons_label.config(text="Lütfen bekleyin...", background='yellow')
        self.btn_pull_push.state(['disabled'])
        t2 = Thread(target=self.gitPullPushHelper, args=(self.new_repo_list,))
        t2.start()

    def gitPullPushHelper(self, repo):
        for f in repo:
            try:
                if self.int_var[f].get():
                    if self.str_var[f].get():
                        commit_text = self.str_var[f].get()
                    else:
                        commit_text = 'Auto Commit'

                    git = pygit.PyGit(f)
                    # print(git.pull())
                    git.pull()
                    res_status = git.status()
                    if 'Changes not staged for commit' in res_status:
                        txt_status = f'{f} eklenecek dosyalar var'
                        self.buttons_label.config(text=txt_status, background='#069622', foreground='#FFFFFF')
                        time.sleep(.5)
                        git.add_all()
                        txt_add_all = f"{f}: Değişiklikler eklendi..."
                        self.buttons_label.config(text=txt_add_all, background='#069622', foreground='#FFFFFF')
                        time.sleep(.5)
                        git.commit(commit_text)
                        txt_commit = f"{f}: Not eklendi..."
                        self.buttons_label.config(text=txt_commit, background='#069622', foreground='#FFFFFF')
                        time.sleep(.5)
                        git.push()
                        txt_push = f"{f}: Repo güncellendi"
                        self.buttons_label.config(text=txt_push, background='#069622', foreground='#FFFFFF')
                        time.sleep(.5)
                    elif 'Already up to date' in res_status:
                        txt_already_upto_date = f"{f}: Zaten güncel"
                        self.buttons_label.config(text=txt_already_upto_date, background='black', foreground='#FFFFFF')
                        time.sleep(.5)
                    else:
                        txt_nothing_done = f"{f}: Herhangi bir işlem yapılmadı."
                        self.buttons_label.config(text=txt_nothing_done, background='red', foreground='#FFFFFF')
                        time.sleep(.5)

            except:
                pass
        time.sleep(1)

        self.buttons_label.config(text="İşlem sonlandı!", foreground='#dddddd')
        self.btn_pull_push.state(['!disabled'])

if __name__ == '__main__':
    master = Tk()
    AutoGit(master)
    master.title('Kolay Git')
    master.geometry('640x480+150+200')
    master.resizable(False, False)
    master.iconbitmap('D:\\Python\\tkinter\\KolayGit\\KolayGit.py')
    master.mainloop()