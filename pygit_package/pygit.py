import subprocess

class PyGit():

    git_dir = r'D:\Python'
    def __init__(self, folder):

        self.git_dir = self.git_dir + '\\' + folder

    def checkRepo(self):
        try:
            git_status = subprocess.check_output(['git', 'status'], cwd=self.git_dir)
            git_status = git_status.decode('utf-8')
            return True
        except:
            return False


    def status(self):
        git_status = subprocess.check_output(['git', 'status'], cwd=self.git_dir)
        git_status = git_status.decode('utf-8')
        return git_status

    def pull(self):
        git_pull = subprocess.check_output(['git', 'pull'], cwd=self.git_dir)
        git_pull = git_pull.decode('utf-8')
        return git_pull

    def add_all(self):
        git_add_all = subprocess.check_output(['git', 'add', '.'], cwd=self.git_dir)
        git_add_all = git_add_all.decode('utf-8')
        return git_add_all

    def commit(self, msg):
        git_list = ['git', 'commit', '-m', msg]
        git_commit = subprocess.check_output(git_list, cwd=self.git_dir)
        git_commit = git_commit.decode('utf-8')
        return git_commit

    def push(self):
        git_push = subprocess.check_output(['git', 'push', 'origin', 'master'], cwd=self.git_dir)
        git_push = git_push.decode('utf-8')
        return git_push
